#include <iostream>
#include "spreadsheet.h"

using namespace std;

int main()
{
  spreadsheet saveTest;

  saveTest.save();

  saveTest.change_cell("A1", "Hello");
  saveTest.change_cell("A2", "World");

  saveTest.save();

  cout << saveTest.undo() << endl;
  cout << saveTest.undo() << endl;
  cout << saveTest.undo() << endl;
  cout << saveTest.undo() << endl;
  cout << saveTest.undo() << endl;
  cout << saveTest.undo() << endl;

  spreadsheet test("XML.ss");

  cout << test.undo() << endl;

  if (test.get_version() != 0)
    cout << "ERROR: VERSION TEST 1" << endl;

  test.change_cell("A3", "!");

  if (test.get_version() != 1)
    cout << "ERROR: VERSION TEST 2" << endl;

  if (test.get_cell("A1").compare("Hello") != 0)
    cout << "ERROR: GET CELL TEST 1" << endl;

  if (test.get_cell("A3").compare("!") != 0)
    cout << "ERROR: GET CELL TEST 2" << endl;

  test.undo();

  if (test.get_cell("A3").compare("") != 0)
    cout << "ERROR: UNDO TEST 1" << endl;

  test.change_cell("A1", "");

  cout << endl << test.get_XML() << endl << endl;

  test.save();

  spreadsheet ErrorTest("ErrorTest", "pass");


  ErrorTest.change_cell("A1", "That");
  ErrorTest.change_cell("A2", "This");
  
  ErrorTest.save();

  ErrorTest.change_cell("A1", "");
  ErrorTest.change_cell("A2", "");
  ErrorTest.change_cell("A3", "");

  ErrorTest.save();

  cout << ErrorTest.undo() << endl;;

  try{
  spreadsheet test2("Doesn'tExist");
  cout << "Failed exception test." << endl;
  } catch (exception e)
    {

    }

  cout << "Tests completed" << endl;

  return 0;
}
