/* This spreadsheet class builds a shell class for the server to use.
 * Contains functions to change cells and generate new spreadsheets.
 * 
 * Author: David Onken
 */

#include "spreadsheet.h"
#include <sstream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>

using namespace std;

/**
 * Used to create a default spreadsheet for testing.
 */
spreadsheet::spreadsheet()
{
  spreadsheet::version = 0;
  spreadsheet::password = "password";
  spreadsheet::name = "XML";
}

/**
 * The main constructor. Takes in a filename and loads the XML file.
 *
 * Throws Exception if filename doesn't exist or is malformed.
 */
spreadsheet::spreadsheet(std::string filename)
{
  spreadsheet::version = 0;
  load(filename);
}

/**
 * New spreadsheet builder. Takes in a filename and password and builds the spreadsheet.
 */
spreadsheet::spreadsheet(std::string filename, std::string password)
{
  spreadsheet::version = 0;
  spreadsheet::name = filename;
  spreadsheet::password = password;
}

/**
 * Destructor
 */
spreadsheet::~spreadsheet()
{

}

/**
 * Get_cell returns a string version of the contents of the input string
 * name. If not found it returns an empty string.
 *
 */
string spreadsheet::get_cell(string name)
{
  // If a cell is not found, return an empty screen. Otherwise the contents of the cell are returned
  if (spreadsheet::map.find(name) == spreadsheet::map.end())
    return "";
  else
    return spreadsheet::map[name];
}

/**
 * Change_cell modifies the input cell name with the new input content.
 * Both arguments are strings. Returns the UPDATE command for other clients when successful.
 */
string spreadsheet::change_cell(string name, string content)
{
  undo_list.push(name);   // Add both the name and content to the stack.
  undo_list.push(spreadsheet::map[name]);

  if (content.compare("") == 0)
    spreadsheet::map.erase(name);
  else
    spreadsheet::map[name] = content;

  // Increment the version number.
  spreadsheet::version++;

  stringstream ss;
  ss << spreadsheet::version;
  string ver  = ss.str(); // Get the version as a string

  ss.str("");
  ss << content.size();
  string siz = ss.str(); // Get the size as a string

  // Return true
  return "UPDATE\nName:" + spreadsheet::name + "\nVersion:" + ver + "\nCell:" + name + "\nLength:" + siz + "\n" + content + "\n";
}

/**
 * Reverts the last changed cell.
 *
 * Returns a protocol-mandated string in the case of success and will return one in if the undo is unsuccessful.
 */
string spreadsheet::undo()
{
  if(undo_list.size() < 2) // If the list size is less than two...
    {
      string ret_str = "UNDO END\nName:";
      ret_str += spreadsheet::name + "\nVersion:";

      stringstream ss;
      ss << spreadsheet::version;
      string ver = ss.str();

      ret_str += ver + "\n";
      return ret_str; // Return the no-more-undos message
    }

  string temp_content = spreadsheet::undo_list.top(); // Put both of the top items into temporary variables/
  spreadsheet::undo_list.pop();
  string temp_cell = spreadsheet::undo_list.top();
  spreadsheet::undo_list.pop();

  if (temp_content.compare("") == 0) // Erase the item if the input is empty.
    spreadsheet::map.erase(temp_cell);
  else
    spreadsheet::map[temp_cell] = temp_content; // If it isn't, replace it.

  // Increment the version number.
  spreadsheet::version++;

  stringstream ss;
  ss << spreadsheet::version; // Get version
  string ver  = ss.str();

  ss.str("");
  ss << temp_content.size(); // Get size
  string siz = ss.str();

  return ("UNDO OK\nName:" + spreadsheet::name + "\nVersion:" + ver + "\nCell:" + temp_cell + "\nLength:" + siz + "\n" + temp_content + "\n"); // Success command
}

/**
 * Private function. Load opens an XML file and parses it into this spreadsheet.
 *
 */
void spreadsheet::load(string filename)
{
  // Temporary namespace and declare a property tree
  using boost::property_tree::ptree;
  ptree pt;

  // Save the filename as this spreadsheet's name.
  spreadsheet::name = filename;

  read_xml(filename, pt); // Build a property tree with the file

  spreadsheet::password = pt.get<string>("spreadsheet.password");


  // Use a boost loop to pull out all the cells.
  BOOST_FOREACH(ptree::value_type const& v, pt.get_child("spreadsheet")) {
      if (v.first == "cell") {
	spreadsheet::map[v.second.get<string>("name")] = v.second.get<string>("contents"); // If the current item is "cell", 
                                                                                           // then assign the items to the spreadsheet.
      }
  }
}

/**
 * Save immediately saves the spreadsheet into an XML file under the name
 * it has stored for itself.
 */
bool spreadsheet::save()
{
  using boost::property_tree::ptree;
  ptree pt;

  while(!undo_list.empty())
	undo_list.pop();

  pt.put("spreadsheet.password", spreadsheet::password);

  for(boost::unordered_map<string, string>::iterator it =spreadsheet::map.begin(); it != spreadsheet::map.end(); ++it) {
    // Build a cell item and add two children to the item, name and cell. Assign the items.
    if (it->second.compare("") == 0) continue;
    ptree & node = pt.add("spreadsheet.cell", "");
    node.put("name", it->first);
    node.put("contents", it->second);
  }

  write_xml(spreadsheet::name + ".ss", pt); // Write the property tree to an XML file

  return true;
}

/**
 * Returns the current XML for this spreadsheet.
 */
string spreadsheet::get_XML()
{
  using boost::property_tree::ptree;
  ptree pt;

  pt.put("spreadsheet.password", spreadsheet::password); // Put a password field

  for(boost::unordered_map<string, string>::iterator it =spreadsheet::map.begin(); it != spreadsheet::map.end(); ++it) {
    // Build a cell item and add two children to the item, name and cell. Assign the items.
    ptree & node = pt.add("spreadsheet.cell", "");
    node.put("name", it->first);
    node.put("contents", it->second);
  }

  stringstream ss; // Open a stringstream to stand in for a file.
  write_xml(ss, pt);

  return ss.str(); // Return a string representation of the stringstream
}


/**
 * Returns the current version of the spreadsheet.
 *
 */
int spreadsheet::get_version() {
  return spreadsheet::version;
}

/**
 * Returns the current password for the spreadsheet.
 *
 */
string spreadsheet::get_password() {
  return spreadsheet::password;
}

/**
 * Returns the current name for the spreadsheet.
 *
 */
string spreadsheet::get_name() {
  return spreadsheet::name;
}

void spreadsheet::set_name(std::string name) {
  spreadsheet::name = name;
}

void spreadsheet::set_password(std::string pass) {
  spreadsheet::password = pass;
}
