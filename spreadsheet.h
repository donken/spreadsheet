/* This is the spreadsheet class containing methods for
 * the server to execute.
 *
 * Author: David Onken
 */

#ifndef SPREADSHEET_H
#define SPREADSHEET_H

#include <string>
#include <boost/unordered_map.hpp>
#include <iostream>
#include <stack>

class spreadsheet {

 public:

  spreadsheet();
  spreadsheet(std::string filename);
  spreadsheet(std::string filename, std::string password); // Build a complete SS
  ~spreadsheet();

  std::string get_cell(std::string name);
  std::string change_cell(std::string name, std::string content);

  int get_version();
  std::string get_password();
  std::string get_name();
  std::string get_XML();

  bool save();

  void set_password(std::string pass);
  void set_name(std::string new_name);

  std::string undo();

 private:

  void load(std::string filename);

  int version;
  std::string name;
  std::string password;

  boost::unordered_map<std::string, std::string> map;
  std::stack<std::string> undo_list;

};

#endif
